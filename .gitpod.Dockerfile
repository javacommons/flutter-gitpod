FROM gitpod/workspace-flutter

USER gitpod

RUN flutter channel stable
RUN flutter upgrade
